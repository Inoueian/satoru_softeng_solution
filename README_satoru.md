MRML.py is a MapReduce program using mrjob to find all the suspicious transactions. In order to produce a list of transactions and a list of suspicious entities(sorted by the number of suspicious transactions), it is run as
```
python MRML.py data/transactions.csv | python make_output.py transactions.txt entities.txt
```
with appropriate options after MRML.py if you wish to run this in a Hadoop environment with full parallelization. It reads each line in the CSV, groups transactions by their date, finds all transactions having the characteristics of bridging, and outputs suspicious transactions in the form
```
('transaction', transaction_id), 1
```
and outputs suspicious entities in the form
```
('entity', entity_id), count
```
count is the number of times that this entity was involved in a suspicious transaction, whether as sender, intermediary, or receiver of the money.

make_output.py turns the output of MRML.py into 2 files. transactions.txt is the list of transaction ID's (1 per line), and entities.txt is the list of entity ID's (1 per line) with the number of suspicious transactions that they were involved in, where the lines are sorted by the number in descending order.
