#!/bin/python

from mrjob.job import MRJob, MRStep

class MRML(MRJob):
    """1st mapper simply groups by timestamp
    2nd mapper finds all triplets (a, b, c)
    where a sends money to b, and b sends 
    90-100% of the money to c on the same day.
    Then reducer counts the number of times
    (a, b, c) took part in such transactions."""
    def steps(self):
        return [MRStep(mapper=self.mapper,
                       reducer=self.reducer),
                MRStep(mapper_init=self.mapper_init1,
                       mapper=self.mapper1,
                       mapper_final=self.mapper_final1,
                       reducer=self.reducer1)]

    def mapper(self, _, line):
        #read line
        transaction, timestamp, amount, sender, receiver = \
                line.strip().split('|')
        #use timestamp as key
        if amount == 'AMOUNT': #1st row
            pass
        else:
            amount = float(amount)
            if sender != receiver: #ignore self transactions
                yield timestamp, (transaction, amount, sender, receiver)

    def reducer(self, key, values):
        yield key, values

    def mapper_init1(self):
        self.set = set()

    def mapper1(self, key, lines):
        sender_set = set(sender for _, _, sender, _ in lines)
        receiver_set = set(receiver for _, _, _, receiver in lines)
        #who appears in both sets?
        both_sides = sender_set.intersection(receiver_set)
        for account in both_sides:
            for transaction1, amount1, sender1, receiver1 in lines:
                if receiver1 == account: #1st leg
                    for transaction2, amount2, sender2, receiver2 in lines:
                        if ((sender2 == account) and
                            (amount2 < amount1) and
                            (amount2 > 0.9*amount1)):
                            #record each transaction, with no double count
                            self.set.add((transaction1,
                                       sender1,
                                       receiver1)) 
                            self.set.add((transaction2,
                                       sender2,
                                       receiver2)) 

    def mapper_final1(self):
        for transaction, sender, receiver in self.set:
            yield ('transaction', transaction), 1
            yield ('entity', sender), 1
            yield ('entity', receiver), 1

    def reducer1(self, key, values):
        yield key, sum(values)

if __name__ == '__main__':
    MRML.run()
