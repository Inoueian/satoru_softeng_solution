"""This Python code makes a file listing suspicious transaction IDs,
and a file of suspicious entities sorted by the number of transactions."""
import sys
import ast

if __name__ == '__main__':
    file1 = open(sys.argv[1], 'w')
    file2 = open(sys.argv[2], 'w')
    entities = []
    for line in sys.stdin:
        #line is in format (text, id), count
        key, value = line.strip().split('\t')
        key = ast.literal_eval(key) #make this a list
        if key[0] == 'transaction':
            file1.write(key[1] + '\n')
        elif key[0] == 'entity':
            entities.append((key[1], value))
    #sort rows by row[1] in descending order
    entities = sorted(entities, key=lambda x: int(x[1]), reverse=True)
    for line in entities:
        file2.write(line[0] + ',' + str(line[1]) + '\n')
    file1.close()
    file2.close()
